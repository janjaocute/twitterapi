using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TwitterApi.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TwitterApi.Controllers
{
   
    [Route("api/[controller]")]
    public class TweetController : Controller
    {
        public class MyUser
        {
            public string sessionID { get; set; }
            public string userID { get; set; }
        }
    /*public class CompareTweet : IComparer<Tweet>
        {
            public int Compare(Tweet a, Tweet b) {

                if(a.WhenCreated > b.WhenCreated) {
                    return -1;
                } else if (a.WhenCreated < b.WhenCreated) {
                    return 1;
                } else {


                    return 0;
                }
            }
        }*/
        public ITweetRepository _tweets { get; set; }
        //public IFollowingRepository _followings {get; set;}
    
        private static HttpClient client = new HttpClient();

        public TweetController(ITweetRepository tweets)
        {
            _tweets = tweets;
        }

        protected string GetUserIdFromSession(string sessionid)
        {
            HttpResponseMessage response = client.GetAsync("http://localhost:5000/api/session/" + sessionid).Result;
            if (response.IsSuccessStatusCode)
            {
                string result = response.Content.ReadAsStringAsync().Result;
                MyUser user = JsonConvert.DeserializeObject<MyUser>(result);
                return user.userID;
            }
            else
            {
                return null;
            }
        }
        
        [HttpGet]
        public IEnumerable<Tweet> Get(DateTime recent, string sessionid)
        {
            if(sessionid != null && sessionid != "" && recent != null) {
                return _tweets.GetAllUserRecent(GetUserIdFromSession(sessionid), recent);
            } else if(recent != null) {
                return _tweets.GetAllRecent(recent);
            }
            return _tweets.GetAll();
        }

        [HttpGet("{id}")]
        public Tweet Get(int id)
        {
            return _tweets.Find(id);
        }

        // Your work here, this function should return list of tweet from user's followings
        [HttpGet("timeline")]
        public IEnumerable<Tweet> GetTimeline(DateTime recent, string sessionid)
        {           
                       
            
                        List<Following> followings = new List<Following>();

                        HttpResponseMessage response = client.GetAsync("http://localhost:5200/api/following/?sessionid="+sessionid).Result;
                        if (response.IsSuccessStatusCode)
                            {
                                string result = response.Content.ReadAsStringAsync().Result;
                                followings = JsonConvert.DeserializeObject<List<Following>>(result);   
                            } 

                        //string userid;
                        List<Tweet> list_tweet = new List<Tweet>();
                        if (sessionid == null || sessionid == "")
                        {
                            return null;
                        }
                        else
                        {
                            foreach(Following following in followings.ToList()){

                                 foreach(Tweet tweet in _tweets.GetAllUserRecent(Convert.ToString(following.Follow),recent))
                                 {
                                        list_tweet.Add(tweet);
                                }
                            }

                            return list_tweet;    

                        }        
            /*   CompareTweet ct = new CompareTweet();       
               tweet_list.sort(ct);


               foreach(Tweet tweet in tweet_list ){

                _tweets.Add(tweet);
                   

               }

               return  _tweets.GetAll();
               */
               
             // return _tweets.GetAll();
                 // find user who are recently on twwet timeline
            // 2. for each following, get their recent tweets.

            // 3. combine them, return.
           
            //*/
            
          //  return null;


        }
        

        [HttpPost]
        public IActionResult Create([FromBody]Tweet tweet, string sessionid)
        {
            if (sessionid == null || sessionid == "")
            {
                return BadRequest();
            }
            tweet.Owner = Int32.Parse(GetUserIdFromSession(sessionid));
            _tweets.Add(tweet);
            return Ok();
        }
    }
}
